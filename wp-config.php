<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'admin' );

/** MySQL database password */
define( 'DB_PASSWORD', 'admin' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'UBb($Q;hdZ%!3=>*BWh#{ONR:2}^QWi3:7PE&p&H*>FZ{4mPSrzS2OOP9G!@FEZ/' );
define( 'SECURE_AUTH_KEY',  ':e;8$Q-U*SUA5?aEX@oB%WF<FO2k}(ot}5)R0v{K9hvNSJ+:{/JoQFtW_C]Zv/rd' );
define( 'LOGGED_IN_KEY',    '/sP5_n e*`W[9/| 4h~*O.rgK?,s-|$~Ia Ow;yFxOqw3]4 Y;XtIz)g8DB+3u`F' );
define( 'NONCE_KEY',        'o$U*<U^M+:s5tJ7tZSVByn@.EF,v4cRtxfDM u<?*,fHG!ugsNmk-?Y,%Pt]AiB+' );
define( 'AUTH_SALT',        '*2~<(Z o]];tjIm q,Mi%mg{c?@,{~m]0 {ntsI[8?dYP >gpk_gK4w7beP<9.kW' );
define( 'SECURE_AUTH_SALT', 'VZpm|[sl3cw)&}CpI],pkTZysXOa|76+kcbA!]<CM0oR@{C#p.>lC?>D(*Jm-t)5' );
define( 'LOGGED_IN_SALT',   't9]|3Ubm&5nSU$G2z8Ae9odyFf_$FVcYV>1&MC|~G*KA!uIK=h0ZxA<t?a;83/i~' );
define( 'NONCE_SALT',       'F9TPVX.hGdTe5TSt8WAGhtx0X#2j_Iteu3+r`/HO;Xrwm8D_ `V)RDkVHt;#~kTZ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
