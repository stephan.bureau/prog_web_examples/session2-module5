<?php
header('Content-Type: application/json;charset=utf-8');
include_once("./bd.php");
$bd = new BD();

// si les variables de formulaire "name" et "address"
$success = false;
if (isset($_POST["name"]) && isset($_POST["address"])) {
  try {
    // on ajoute le nouveau contact à la base de données
    $stmt = $bd->pdo->prepare('INSERT INTO contact (name, address) VALUES(:name, :address)');
    $stmt->bindParam('name', $_POST["name"], PDO::PARAM_STR);
    $stmt->bindParam('address', $_POST["address"], PDO::PARAM_STR);
    $success = $stmt->execute();
  } catch (Exception $e) {
    throw $e;
  }
}
header('Status: 200');

$result = array("result" => $success);
echo json_encode($result);
