<?php
include_once(get_template_directory() . "/bdd/exceptions.php");

class BD
{
    private $host = '127.0.0.1';
    private $db   = 'testdb1';
    private $user = 'admin';
    private $pass = 'admin';
    private $charset = 'UTF8';
    private $connectionString;

    public $pdo;

    public function __construct()
    {
        $this->connectionString =
            "mysql:host=$this->host;dbname=$this->db;charset=$this->charset";
        $this->init();
    }

    private function init()
    {
        try {
            $this->pdo = new PDO($this->connectionString, $this->user, $this->pass);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            throw new PDOException($e->getMessage(), (int) $e->getCode());
        }
    }

    public function getContactsList()
    {
        $stmt = $this->pdo->prepare('SELECT name FROM contact');
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function ajoutContact(string $name, string $address)
    {
        try {
            $stmt = $this->pdo->prepare('INSERT INTO contact (name, address) VALUES(:name, :address)');
            $stmt->bindParam('name', $name, PDO::PARAM_STR);
            $stmt->bindParam('address', $address, PDO::PARAM_STR);
            return $stmt->execute();
        } catch (Exception $e) {
            echo $e;
            // throw new MissingFieldException($e);
        }
    }
}
