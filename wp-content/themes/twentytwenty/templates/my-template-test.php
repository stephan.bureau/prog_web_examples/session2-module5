<?php /* Template Name: MonTemplateTest123 */ ?>
<?php
get_header();
include_once(get_template_directory() . "/bdd/bd.php");
$bd = new BD();
?>
<main id="site-content" role="main">

  <h2 id="form-message"></h2>
  <i>Nom et adresse du nouveau contact :</i>
  <form onsubmit="ajoutAjax(); return false" method="POST">
    <input id="form-name" name="name" type="text" placeholder="Nom du contact" />
    <input id="form-address" name="address" type="text" placeholder="Adresse du contact" />
    <input type="submit" value="Créer">
  </form>
  <script>
    async function ajoutAjax() {
      // console.log("ceci est un test");
      if (window.fetch) {
        const name = document.querySelector("#form-name").value;
        const address = document.querySelector("#form-address").value;
        const body = new URLSearchParams(`action=ajoutContact&name=${name}&address=${address}`);
        try {
          const response = await fetch("<?php echo admin_url('admin-ajax.php'); ?>", {
            method: "POST",
            body: body
          });
          if (response.ok) {
            const data = await response.json();
            console.log(data.result)
            document.querySelector("#form-message").innerHTML = "Nouveau contact créé avec succès!";
          }
        } catch (err) {
          console.error(err);
        }
      } else {
        console.log("fetch is not supported by your browser");
      }
    }
  </script>

</main><!-- #site-content -->

<?php get_footer(); ?>